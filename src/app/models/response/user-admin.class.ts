export interface UserAdmin{
    id: number;
    username : string;
    password : string;
    dob : string;
    fullName : string;
    avatar : string;
    timeUpdate: string;
}