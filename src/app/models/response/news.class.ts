import { NewsCategory } from '../request/category-new.class';

export class News{
    id: number;
    title : string;
    description : string;
    image: string;
    content: string;
    categoryId: number;
    category : NewsCategory;
    timeUpdate: string;
}