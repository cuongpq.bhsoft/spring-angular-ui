import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {  MatDialogModule } from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { AppComponent } from './app.component';
import { AdminLoginComponent } from './component/admin/admin-login/admin-login.component';
import { DashboardComponent } from './component/admin/dashboard/dashboard.component';
import { AdminComponent } from './component/admin/admin.component';
import { ClientComponent } from './component/client/client.component';
import { HomeComponent } from './component/client/home/home.component';

import { RouterModule } from '@angular/router'
import { NotifierModule, NotifierOptions } from 'angular-notifier'
import { HttpClientModule } from '@angular/common/http'
import { appRoutes } from './router/app-router.class'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminHeaderComponent } from './component/admin/admin-header/admin-header.component';
import { AdminFooterComponent } from './component/admin/admin-footer/admin-footer.component';
import { AdminServiceService } from './service/admin-service.service';
import { AdminNewsComponent } from './component/admin/admin-news/admin-news.component';
import { InitNewComponent } from './component/admin/admin-news/init-new/init-new.component';
import { CategoryInitNewComponent } from './component/admin/admin-news/category-init-new/category-init-new.component';
import { CategoryNewComponent } from './component/admin/admin-news/category-new/category-new.component';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';
import { LoadingComponent } from './component/loading/loading.component';
import { ConfirmDialogComponent } from './component/confirm-dialog/confirm-dialog.component';
const customNotifierOptions: NotifierOptions = {
  position: {
		horizontal: {
			position: 'middle',
			distance: 12
		},
		vertical: {
			position: 'top',
			distance: 12,
			gap: 10
		}
	},
  theme: 'material',
  behaviour: {
    autoHide: 5000,
    onClick: 'hide',
    onMouseover: 'pauseAutoHide',
    showDismissButton: true,
    stacking: 4
  },
  animations: {
    enabled: true,
    show: {
      preset: 'slide',
      speed: 300,
      easing: 'ease'
    },
    hide: {
      preset: 'fade',
      speed: 300,
      easing: 'ease',
      offset: 50
    },
    shift: {
      speed: 300,
      easing: 'ease'
    },
    overlap: 150
  }
};
@NgModule({
  declarations: [
    AppComponent,
    AdminLoginComponent,
    DashboardComponent,
    AdminComponent,
    ClientComponent,
    HomeComponent,
    AdminHeaderComponent,
    AdminFooterComponent,
    AdminNewsComponent,
    InitNewComponent,
    CategoryInitNewComponent,
    CategoryNewComponent,
    PageNotFoundComponent,
    LoadingComponent,
    ConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CKEditorModule,
    MatDialogModule,
    NotifierModule.withConfig(customNotifierOptions)
  ],
  providers: [
    AdminServiceService
  ],
  
  bootstrap: [AppComponent],
  entryComponents:[ConfirmDialogComponent]
})
export class AppModule { }
