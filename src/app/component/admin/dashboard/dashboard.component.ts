import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { AdminServiceService } from 'src/app/service/admin-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit,AfterContentChecked {

  constructor(
    private adminService : AdminServiceService
  ) { }

  ngOnInit(): void {
    this.adminService.isLoading= false;
  }
  ngAfterContentChecked(){
  }
}
