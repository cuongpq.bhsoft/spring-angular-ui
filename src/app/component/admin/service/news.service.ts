import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BaseResponse } from 'src/app/models/response/base-response.class';
import { NewsCategory } from 'src/app/models/request/category-new.class';
import { News } from 'src/app/models/response/news.class';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private apiCategory = "/api/category";
  private apiNew = "/api/new";
  private token: string;
  private listCategory: NewsCategory[] = [];
  private listNews: News[] = [];
  static header1: HttpHeaders;
  constructor(
    private http: HttpClient
  ) {
    this.token = localStorage.getItem("token");
  }
  set _listCategory(list: any[]) {
    this.listCategory = list;
  }
  get _listCategory() {
    return this.listCategory;
  }

  set _listNews(list: any[]) {
    this.listNews = list;
  }
  get _listNews() {
    return this.listNews;
  }
  getCategoryById(id: number): NewsCategory {
    if (this.listCategory) {
      return this.listCategory.find(item => item.id === id);
    } else {
      return null;
    }
  }
  getNewById(id: number): News {
    if (this.listNews) {
      return this.listNews.find(item => item.id === id);
    } else {
      return null;
    }
  }
  doGetAllNews(): Observable<BaseResponse> {
    let api = `${this.apiNew}/getAllNews`;
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    headers = headers.append('Authorization', this.token);
    return this.http.get<BaseResponse>(api, { headers });
  }

  doGetAllNewsCategory(): Observable<BaseResponse> {
    let api = `${this.apiCategory}/all-category`;
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    headers = headers.append('Authorization', this.token);
    return this.http.get<BaseResponse>(api, { headers });
  }

  doSaveCategory(request: NewsCategory): Observable<BaseResponse> {
    let api = `${this.apiCategory}/save-category`;
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Authorization', this.token);
    return this.http.post<BaseResponse>(api, request, { headers });
  }
  doDeleteCategory(id: number) {
    let api = `${this.apiCategory}/delete`;
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Authorization', this.token);
    let params: HttpParams = new HttpParams();
    params = params.append("idCategory", id.toString())
    return this.http.delete<BaseResponse>(api, { headers, params });
  }

  doDeleteNew(id: number) {
    let api = `${this.apiNew}/delete`;
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Authorization', this.token);
    let params: HttpParams = new HttpParams();
    params = params.append("id", id.toString())
    return this.http.delete<BaseResponse>(api, { headers, params });
  }

  doGetCategoryById(id: number) {
    let api = `${this.apiCategory}/${id}`;
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Authorization', this.token);
    let params: HttpParams = new HttpParams();
    params = params.append("idCategory", id.toString())
    return this.http.get<BaseResponse>(api, { headers, params });
  }
  doSaveNew(request: News) {
    let api = `${this.apiNew}/save`;
    let headers = Header.getHeader();
    return this.http.post<BaseResponse>(api, request, { headers });
  }
}
export class Header {
  static getHeader(): HttpHeaders {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Authorization', localStorage.getItem("token"));
    return headers;
  }
}