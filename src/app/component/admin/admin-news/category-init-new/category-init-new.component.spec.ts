import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryInitNewComponent } from './category-init-new.component';

describe('CategoryInitNewComponent', () => {
  let component: CategoryInitNewComponent;
  let fixture: ComponentFixture<CategoryInitNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryInitNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryInitNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
