import { Component, OnInit, ViewChild, AfterContentInit } from '@angular/core';
import { NewsCategory } from 'src/app/models/request/category-new.class';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { BaseResponse } from 'src/app/models/response/base-response.class';
import { Router, ActivatedRoute } from '@angular/router';
import { NewsService } from '../../service/news.service';
import { AdminServiceService } from 'src/app/service/admin-service.service';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { Route } from '@angular/compiler/src/core';
@Component({
  selector: 'app-category-init-new',
  templateUrl: './category-init-new.component.html',
  styleUrls: ['./category-init-new.component.css']
})
export class CategoryInitNewComponent implements OnInit,AfterContentInit {
  public newData; 
  public Editor = ClassicEditor;
  public categoryInfo = new NewsCategory();
  private id : number;

  constructor(
    private newService: NewsService,
    private adminService: AdminServiceService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
   
  }

  ngAfterContentInit(){
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
    if(this.id){
      this.getCategory(this.id);
    }
    this.validateForm();
  }
  getCategory(id: number) {
    this.categoryInfo = this.newService.getCategoryById(id);
  }

  validateForm() {
    this.newData = new FormGroup({
      title: new FormControl(this.categoryInfo? this.categoryInfo.title : "", Validators.compose([
        Validators.required,
        Validators.minLength(3)
      ])),
      description: new FormControl(this.categoryInfo? this.categoryInfo.description : "", Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ])),
      content: new FormControl(this.categoryInfo? this.categoryInfo.content : "", Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ]))
    });
  }


  doSaveCategoryNew(val) {
    this.adminService.isLoading = true;
    let category = new NewsCategory();
    category.title = val.title;
    category.content = val.content;
    category.description = val.description;
    if(this.id){
      category.id = this.id;
    }
    this.newService.doSaveCategory(category).subscribe(
      (baseResponse: BaseResponse) => {
        this.onSuccessSaveCategory(baseResponse);
      }, (throwable) => {
        this.onErrorSaveCategory(throwable);
      }
    )
  }

  onSuccessSaveCategory(baseResponse: BaseResponse) {
    if (baseResponse.code == 0) {
      this.adminService.showNotification("success", baseResponse.message);
      this.adminService.isLoading = false;
      this.router.navigate(['/admin/category-new']);
    } else {
      this.adminService.isLoading = false;
      this.adminService.showNotification("error", baseResponse.message);
    }
  }
  onErrorSaveCategory(throwable) {
    this.adminService.isLoading = false;
    this.adminService.showNotification("success", "Thêm danh mục thất bại, vui lòng thử lại!");
    this.router.navigate(['page-not-found']);
  }
}
