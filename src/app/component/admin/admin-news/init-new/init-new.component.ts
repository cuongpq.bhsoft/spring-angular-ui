import { Component, OnInit, AfterViewChecked, AfterViewInit } from '@angular/core';
import { FormGroup, Validators,FormControl } from '@angular/forms';
import { CKEditorModule } from  '@ckeditor/ckeditor5-angular';
import { News } from 'src/app/models/response/news.class';
import { NewsService } from '../../service/news.service';
import { BaseResponse } from 'src/app/models/response/base-response.class';
import { NewsCategory } from 'src/app/models/request/category-new.class';
import { AdminServiceService } from 'src/app/service/admin-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfirmDialogService } from 'src/app/service/confirm-dialog.service';

@Component({
  selector: 'app-init-new',
  templateUrl: './init-new.component.html',
  styleUrls: ['./init-new.component.css']
})
export class InitNewComponent implements OnInit,AfterViewInit {
  public editorValue: string = '';
  public itemNew: FormGroup;
   itemInfo : News;
  public listCategory : NewsCategory[] = [];
  private id : number;
  constructor(
    private newService: NewsService,
    private adminService: AdminServiceService,
    private router: Router,
    private dialogService: ConfirmDialogService,
    private route : ActivatedRoute
  ) { }

  public ngOnInit(): void {
    this.adminService.isLoading = true;
    this.id = parseInt(this.route.snapshot.paramMap.get('id'));
    if(this.id){
      this.itemInfo = this.newService.getNewById(this.id);
    }
    this.doGetAllCategory();
    this.validatorsForm();
  }
  
  ngAfterViewInit(){
   
   
  }
  public doGetAllCategory(){
    this.newService.doGetAllNewsCategory().subscribe(
      (data: BaseResponse)=>{
        this.onSuccessGetAllCategory(data);
      },error=>{
        this.onErrorGetAllCategory(error);
      }
    )
  }
  public onSuccessGetAllCategory(baseResponse: BaseResponse){
    if(baseResponse.code==0){
      this.listCategory = baseResponse.data;
      this.adminService.isLoading = false;
    }else{
      this.adminService.showNotification("error", baseResponse.message);
      this.router.navigate(["admin"]);
      this.adminService.isLoading = false;
    }
  }

  public onErrorGetAllCategory(error: any){
    this.adminService.isLoading = false;
    this.adminService.showNotification("success", "Thêm danh mục thất bại, vui lòng thử lại!");
    this.router.navigate(['page-not-found']);
  }
  public validatorsForm(){
    this.itemNew = new FormGroup({
      title: new FormControl(this.itemInfo ? this.itemInfo.title : "", Validators.compose([
        Validators.required,
        Validators.minLength(3)
      ])),
      description: new FormControl(this.itemInfo ? this.itemInfo.description : "", Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ])),
      content: new FormControl(this.itemInfo ? this.itemInfo.content : "", Validators.compose([
        Validators.required,
        Validators.minLength(5)
      ])),
      category: new FormControl()
    })
  }
  doSaveNew(data){
    let request = new News();
    request.title = data.title;
    request.description = data.description;
    request.content = data.content;
    request.category = this.listCategory.find(item => item.id === parseInt(data.category));
    if(this.id){
      request.id = this.id;
    }
    this.newService.doSaveNew(request).subscribe(
      (data: BaseResponse)=>{
        this.onSuccessSaveNew(data);
      },error=>{
        this.onErrorSaveNew(error);
      }
    )
  }
  onSuccessSaveNew(data : BaseResponse){
    if(data.code==0){
        this.adminService.showNotification("success",data.message);
        this.router.navigate(["admin/admin-news"])
    }else{
      this.adminService.showNotification("error",data.message);
    }
  }
  onErrorSaveNew(error: any){
    this.dialogService.openConfirmDialog("Có lỗi xảy ra, trở về trang đăng nhập !")
    .afterClosed()
    .subscribe(
      (res)=>{
        if(res){
          this.adminService.doLogout();
        }
      }
    )
  }
}
